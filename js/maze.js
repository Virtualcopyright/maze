//Global Variables:
const map = [
    "WWWWWWWWWWWWWWWWWWWWW",
    "W   W     W     W W W",
    "W W W WWW WWWWW W W W",
    "W W W   W     W W   W",
    "W WWWWWWW W WWW W W W",
    "W         W     W W W",
    "W WWW WWWWW WWWWW W W",
    "W W   W   W W     W W",
    "W WWWWW W W W WWW W F",
    "S     W W W W W W WWW",
    "WWWWW W W W W W W W W",
    "W     W W W   W W W W",
    "W WWWWWWW WWWWW W W W",
    "W       W       W   W",
    "WWWWWWWWWWWWWWWWWWWWW",
];
let lastThing = 'start';

init();


function init () {
    //reset stuff:
    let node = document.getElementsByClassName('maze')[0];
    while (node.hasChildNodes()) {
        node.removeChild(node.lastChild);
    }
    document.getElementsByClassName("alert")[0].innerHTML = "<strong>Solve!</strong> Find the way out of the maze!";
    document.documentElement.style.setProperty('--panel-color', '#2196F3');
    lastThing = "start";
    document.addEventListener('keydown', keydown);

    //Row
    for (let y = 0; y < map.length; y++) {
        let row = document.createElement('div');
        row.classList.add('row');

        //Cell
        for (let x = 0; x < map[y].length; x++) {
            let cell = document.createElement('div');
            cell.classList.add('cell');
            if (map[y][x] == 'W')
                cell.classList.add('wall');
            else if (map[y][x] == 'S')
                cell.classList.add('player');
            else if (map[y][x] == 'F')
                cell.classList.add('finish');
            else
                cell.classList.add('open');

            row.append(cell);
        }
        document.getElementsByClassName('maze')[0].append(row);
    }
}

function keydown (e) {
    const direction = e.key;

    moveRequest(direction);
}

function moveRequest (direction) {
    const maze = getMaze();
    const playerPos = getPlayer(maze);
    

    switch (direction) {
        case 'ArrowDown':
            if (!maze[playerPos.y+1][playerPos.x].classList.contains('wall'))
                move(playerPos, {y: playerPos.y+1,x:playerPos.x}, maze);
        break;
        case 'ArrowUp':
            if (!maze[playerPos.y-1][playerPos.x].classList.contains('wall'))
                move(playerPos, {y: (playerPos.y-1),x:playerPos.x}, maze);
        break;
        case 'ArrowLeft':
            if (!maze[playerPos.y][playerPos.x-1].classList.contains('wall'))
                move(playerPos, {y: playerPos.y,x:playerPos.x-1}, maze);
        break;
        case 'ArrowRight':
            console.log(playerPos)
            if (!maze[playerPos.y][playerPos.x+1].classList.contains('wall'))
                move(playerPos, {y: playerPos.y, x:playerPos.x+1}, maze);
        break;
    }
}
function move (currPos, tarPos) {
    const maze = getMaze();

    maze[currPos.y][currPos.x].classList.remove('player');
    maze[currPos.y][currPos.x].classList.add(lastThing);


    lastThing = maze[tarPos.y][tarPos.x].classList[1];
    maze[tarPos.y][tarPos.x].classList.remove(lastThing);
    maze[tarPos.y][tarPos.x].classList.add('player');

    checkWin();
}

function checkWin () {
    if (lastThing == 'finish') {
        document.getElementsByClassName("alert")[0].innerHTML = "<strong>Success!</strong> You solved this maze puzzle!";
        document.documentElement.style.setProperty('--panel-color', '#4CAF50');

        document.removeEventListener('keydown', keydown);
    }
}

function getPlayer () {
    const maze = getMaze();

    for (let y = 0; y < maze.length; y++)
        for (let x = 0; x < maze[y].length; x++)
            if (maze[y][x].classList.contains('player'))
                return {y: y, x: x};
}
function getMaze () {
    let temp = Array.from(document.getElementsByClassName('row'));
    let maze = [];
    for (let y = 0; y < temp.length; y++)
        maze.push(temp[y].childNodes);

    return maze;
}